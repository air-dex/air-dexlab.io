/**
 * @class FadeInAnimatable
 * @brief Class handling the "fadeIn" animation defined in /source/assets/scss/base/animations/_fade-in.scss
 * Elements which are potentially animatable by "fadeIn" are marked with the
 * class whose name is defined in this.animClass. Add the class defined in
 * this.triggerClass to trigger the animation.
 * 
 * Typically the animation is used for a set of elements. The animation is
 * applied to a subset of them which are not contained in the viewport. Then the
 * animation is triggered when those elements enters in the viewport.
 */
class FadeInAnimatable {
	constructor(jq, animClass, triggerClass) {
		// jQuery entity representing elements to animate
		this.$ = jq;
		// Class to mark this.$ as animatable.
		this.animClass = animClass;
		// Class used to trigger the animation by adding it to this.$
		this.triggerClass = triggerClass;
	}

	// Adapted from: https://stackoverflow.com/questions/123999/how-can-i-tell-if-a-dom-element-is-visible-in-the-current-viewport/7557433#7557433
	// TODO : factoriser
	isInViewport() {
		let rect = this.$[0].getBoundingClientRect()
		
		return (
			   rect.top >= 0
			&& rect.left >= 0
			&& rect.bottom <= (window.innerHeight || document.documentElement.clientHeight)
			&& rect.right <= (window.innerWidth || document.documentElement.clientWidth)
		);
	}

	resetAnimation() {
		this.$.removeClass(this.animClass)
		this.$.removeClass(this.triggerClass)
	}

	setAnimation() {
		this.resetAnimation(this.animClass, this.triggerClass)

		if(!this.isInViewport()) {
			this.$.addClass(this.animClass)
		}
	}

	animate() {
		this.$.addClass(this.triggerClass)
	}

	triggerAnimation() {
		if (this.isInViewport()) {
			this.animate()
		}
	}
}
