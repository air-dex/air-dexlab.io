// Because JS static class fields are not enough implemented in browsers (71% as of August 2020).
const SKILL_ANIM_CLASS = 'skill--animated'
const TRIGGER_ANIM_CLASS = 'skill--fade-in'

class Skill {
	constructor(skill) {
		if (typeof skill === 'string') {
			this.$ = $(`div.skill[data-skill-id="${skill}"]`)
		}
		else if (skill instanceof HTMLElement) {
			this.$ = $(skill)
		}
		else if (skill instanceof jQuery) {
			this.$ = skill
		}
		else {
			throw `Cannot build skill with this argument: ${skill.toString()}`
		}

		this.id = this.$.attr('data-skill-id').trim()
		this.name = this.$.children('.skill__name').text().trim()
		this.usedThere = this.$.is('[data-used-there]')
		this.tags = new Set(this.$.attr("data-skill-tags").split(' '))
		this.rate = new Number(this.$.attr("data-skill-rate"))
		this.fadeInAnimation = new FadeInAnimatable(this.$, SKILL_ANIM_CLASS, TRIGGER_ANIM_CLASS)
	}

	applyDisplay(display) {
		this.$.css('display', display)
	}

	match(name, usedThere, tags, lowRate, highRate) {
		// Filter by name
		if (name.length > 0 && !this.nameMatches(name)) {
			return false;
		}

		// Filter by usedThere
		if (usedThere && !this.usedThere) {
			return false;
		}

		// Filter by tags
		if (tags.size > 0 && !this.hasCommonTags(tags)) {
			return false;
		}

		// Last filter: Rate
		return this.rate >= lowRate && this.rate <= highRate;
	}

	hasCommonTags(tags) {
		let commonTags = new Set([...this.tags].filter(tag => tags.has(tag)));
		return commonTags.size > 0
	}

	nameMatches(name) {
		return this.id.toLowerCase().includes(name.toLowerCase())
			|| this.name.toLowerCase().includes(name.toLowerCase());
	}

	eq(otherSkill) {
		if (!(otherSkill instanceof Skill)) {
			throw `We compare skills, not ${typeof otherSkill}: ${otherSkill}.`
		}
		return this.id === otherSkill.id
	}

	isInViewport() {
		return this.fadeInAnimation.isInViewport();
	}

	resetAnimation() {
		this.fadeInAnimation.resetAnimation()
	}

	setAnimation() {
		this.fadeInAnimation.setAnimation()
	}

	triggerAnimation() {
		this.fadeInAnimation.triggerAnimation()
	}
}

// TODO : factoriser pour animation

// @param callback Function which takes a skill as argument and returns nothing.
function doOnAllSkills(callback) {
	$('div.skill')
		.map((index, elt) => new Skill(elt))
		.each((index, skill) => callback(skill))
}

function setUpSkills() {
	doOnAllSkills(skill => skill.setAnimation())
}

function initSkills() {
	setUpSkills()

	var currentWidth = new Number(window.innerWidth);
	var currentHeight = new Number(window.innerHeight);

	$(window).scroll(() => {
		doOnAllSkills(skill => skill.triggerAnimation())
	})

	$(window).resize(() => {
		let newWidth = new Number(window.innerWidth);
		let newHeight = new Number(window.innerHeight);
		
		if (newWidth !== currentWidth || newHeight !== currentHeight) {
			setUpSkills()
		}

		currentWidth = newWidth
		currentHeight = newHeight
	})
}
