class SkillRateSlider{
	constructor(slider) {
		if (typeof slider === 'string') {
			this.$ = $(slider)
		}
		else if (slider instanceof HTMLElement) {
			this.$ = $(slider)
		}
		else if (slider instanceof jQuery) {
			this.$ = slider
		}
		else {
			throw `Cannot build slider with this argument: ${slider.toString()}`
		}
	}

	rates() {
		const RATE_REGEX = /^(?<low>[0-5]),(?<high>[0-5])$/;
		let skrateVal = this.$.slider('getValue');
		let matches = RATE_REGEX.exec(skrateVal.join(','));

		if (matches === null) {
			throw `${skrateVal} n'est pas une valeur valide pour le niveau.`;
		}
		else {
			return {
				low: matches.groups['low'],
				high: matches.groups['high']
			}
		}
	}

	setValue(lowRate, highRate) {
		lowRate = new Number(lowRate)
		highRate = new Number(highRate)
		let minval = Math.min(lowRate, highRate)
		let maxval = Math.max(lowRate, highRate)
		this.$.slider('setValue', [minval, maxval])
	}
}

function initSlider(selector, opts = {}) {
	$(selector).slider(opts)
}
