class SkillFilterForm {
	constructor(form, nameFieldID, utFieldID, tagsFieldName, srsFieldID) {
		if (typeof form === 'string') {
			this.$ = $(form)
		}
		else if (form instanceof HTMLElement) {
			this.$ = $(form)
		}
		else if (form instanceof jQuery) {
			this.$ = form
		}
		else {
			throw `Cannot build form with this argument: ${form.toString()}`
		}

		this.id = this.$.attr('id') || ''
		this.fields = {
			name: nameFieldID,
			ut: utFieldID,
			tags: tagsFieldName,
			rate: srsFieldID
		}
	}

	name() {
		return $(`#${this.id} #${this.fields.name}`).val() || '';
	}

	usedThere() {
		return $(`#${this.id} #${this.fields.ut}`).is(':checked');
	}

	tags() {
		let tags = [];
		$(`#${this.id} input[name=${this.fields.tags}]:checked`).each(function () {
			tags.push($(this).val());
		});
		return tags;
	}

	rates() {
		return this.getSRS().rates()
	}

	filterSkills(skillDisplay = 'flex') {
		let name = this.name()
		if (!(typeof name === 'string')) {
			throw `Name not string : ${name}`
		}

		let usedThere = this.usedThere()
		if (!(typeof usedThere === 'boolean')) {
			throw `Wrong used there : ${usedThere}`
		}
		
		let tags = new Set(this.tags());
		let rates = this.rates();
		let lowRate = new Number(rates.low);
		let highRate = new Number(rates.high);

		$('div.skill').each(function() {
			let skill = new Skill(this)
			skill.applyDisplay(skill.match(name, usedThere, tags, lowRate, highRate) ? skillDisplay : 'none')
		})
	}

	getSRS() {
		return new SkillRateSlider(`#${this.id} #${this.fields.rate}`)
	}

	setRatesValue(lowRate, highRate) {
		this.getSRS().setValue(lowRate, highRate)
	}
}

function initSkillFilterForm(
	form, nameFieldID, utFieldID, tagsFieldName, srsFieldID,
	lowerResetRate, higherResetRate, skillDisplay
) {
	let sff = new SkillFilterForm(form, nameFieldID, utFieldID, tagsFieldName, srsFieldID)
	sff.$.submit(evt => {
		evt.preventDefault();
		sff.filterSkills();
	})
	sff.$.on('reset', () => {
		sff.setRatesValue(lowerResetRate, higherResetRate)
		$('div.skill').css('display', skillDisplay)
	})
}

function setToggleText(toggleSel, text) {
	$(toggleSel).text(text)
}

function initToggleButton(toggleSel, collapsedSel) {
	const COLLAPSED_TOGGLE = '\u25BC'
	const COLLAPSE_TOGGLE = '\u25B2'

	$(collapsedSel).on('shown.bs.collapse', () => setToggleText(toggleSel, COLLAPSE_TOGGLE))
	$(collapsedSel).on('hidden.bs.collapse', () => setToggleText(toggleSel, COLLAPSED_TOGGLE))

	setToggleText(
		toggleSel,
		$(`${collapsedSel}.collapse`).hasClass('show') ? COLLAPSE_TOGGLE : COLLAPSED_TOGGLE
	)
}
