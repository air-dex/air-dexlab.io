class VcardDownloader {
	constructor(form, verfieldID, dlLinkID) {
		if (typeof form === 'string') {
			this.$ = $(form)
		}
		else if (form instanceof HTMLElement) {
			this.$ = $(form)
		}
		else if (form instanceof jQuery) {
			this.$ = form
		}
		else {
			throw `Cannot build form with this argument: ${form.toString()}`
		}

		this.id = this.$.attr('id') || ''
		this.verfield = verfieldID
		this.dlLinkID = dlLinkID
	}

	versionSelect() {
		return $(`#${this.id} #${this.verfield}`)
	}

	hiddenLink() {
		return $(`#${this.id} a#${this.dlLinkID}`)
	}

	version() {
		return this.versionSelect().val()
	}

	vcardHref() {
		return `assets/res/Romain_Ducher.${this.version()}.vcf`
	}

	updateVcardHref() {
		this.hiddenLink().attr('href', this.vcardHref())
	}
}

function initVcardDownloader(formID, selectID, hiddenLinkID) {
	let vdl = new VcardDownloader(formID, selectID, hiddenLinkID)

	vdl.versionSelect().on('change', () => {
		vdl.updateVcardHref()
	})
	vdl.updateVcardHref()

	vdl.$.on('submit', function (evt) {
		evt.preventDefault()
		//vdl.hiddenLink().click()
	})
}
