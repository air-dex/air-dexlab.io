// TODO : factoriser avec animation et skills
// Because JS static class fields are not enough implemented in browsers (71% as of August 2020).
const IT_PROJECT_ANIM_CLASS = 'it-project--animated'
const IT_PROJECT_TRIGGER_CLASS = 'it-project--fade-in'

class ITProject {
	constructor(itproj) {
		if (itproj instanceof HTMLElement) {
			this.$ = $(itproj)
		}
		else if (itproj instanceof jQuery) {
			this.$ = itproj
		}
		else {
			throw `Cannot build IT project with this argument: ${itproj.toString()}`
		}

		this.fadeInAnimation = new FadeInAnimatable(
			this.$,
			IT_PROJECT_ANIM_CLASS,
			IT_PROJECT_TRIGGER_CLASS
		)
	}

	isInViewport() {
		return this.fadeInAnimation.isInViewport();
	}

	resetAnimation() {
		this.fadeInAnimation.resetAnimation()
	}

	setAnimation() {
		this.fadeInAnimation.setAnimation()
	}

	triggerAnimation() {
		this.fadeInAnimation.triggerAnimation()
	}
}

// @param callback Function which takes a skill as argument and returns nothing.
function doOnAllItProjects(callback) {
	$('div.it-project')
		.map((index, elt) => new ITProject(elt))
		.each((index, itproj) => callback(itproj))
}

function setUpItProjects() {
	doOnAllItProjects(itproj => itproj.setAnimation())
}

function initItProjects() {
	setUpItProjects()

	var currentWidth = new Number(window.innerWidth);
	var currentHeight = new Number(window.innerHeight);

	$(window).scroll(() => {
		doOnAllItProjects(skill => skill.triggerAnimation())
	})

	$(window).resize(() => {
		let newWidth = new Number(window.innerWidth);
		let newHeight = new Number(window.innerHeight);
		
		if (newWidth !== currentWidth || newHeight !== currentHeight) {
			setUpItProjects()
		}

		currentWidth = newWidth
		currentHeight = newHeight
	})
}
