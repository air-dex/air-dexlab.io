const ADVICES_SECTION_SEL = 'section.advice-section'
const SOFTWARES_SEL = 'div.soft-advice'

class FilterAdvicesForm {
	constructor(form, platformsFieldName) {
		if (typeof form === 'string') {
			this.$ = $(form)
		}
		else if (form instanceof HTMLElement) {
			this.$ = $(form)
		}
		else if (form instanceof jQuery) {
			this.$ = form
		}
		else {
			throw `Cannot build form with this argument: ${form.toString()}`
		}

		this.id = this.$.attr('id') || ''
		this.platformsFieldName = platformsFieldName
	}

	ostags() {
		let tags = [];
		$(`#${this.id} input[name=${this.platformsFieldName}]:checked`).each(function () {
			tags.push($(this).val());
		});
		return tags;
	}

	filterAdvices(adviceDisplay = 'flex', advicesSectionDisplay = 'flex') {
		let oses = new Set(this.ostags())

		$(SOFTWARES_SEL).each(function() {
			let softAdv = new SoftwareAdvice(this)
			softAdv.applyDisplay(softAdv.match(oses) ? adviceDisplay : 'none')
		})

		// Hide empty software categories section
		displayWholeAdvicesSections(advicesSectionDisplay)
	}
}

function initFilterAdvicesForm(form, platformsFieldName, adviceDisplay, advicesSectionDisplay) {
	let faform = new FilterAdvicesForm(form, platformsFieldName)
	faform.$.submit(evt => {
		evt.preventDefault();
		faform.filterAdvices(adviceDisplay, advicesSectionDisplay);
	})
	faform.$.on('reset', () => {
		$(SOFTWARES_SEL).css('display', adviceDisplay)
		$(ADVICES_SECTION_SEL).css('display', advicesSectionDisplay)
	})
}

function displayWholeAdvicesSections(advicesSectionDisplay = 'flex') {
	const DATA_SCAT_DATANAME = 'data-scat'

	$(ADVICES_SECTION_SEL).each(function () {
		let $this = $(this)
		let advSoftSel = `${ADVICES_SECTION_SEL}[${DATA_SCAT_DATANAME}='${$this.attr(DATA_SCAT_DATANAME)}'] ${SOFTWARES_SEL}`
		let advicedSoftwares = $(advSoftSel).filter(function() {
			let softAdv = new SoftwareAdvice(this)
			return softAdv.isAdviced()
		})
		$this.css('display', advicedSoftwares.length > 0 ? advicesSectionDisplay : 'none')
	})
}

// Toggle button
// TODO: factoriser avec le skill filter form
function setToggleText(toggleSel, text) {
	$(toggleSel).text(text)
}

function initToggleButton(toggleSel, collapsedSel) {
	const COLLAPSED_TOGGLE = '\u25BC'
	const COLLAPSE_TOGGLE = '\u25B2'

	$(collapsedSel).on('shown.bs.collapse', () => setToggleText(toggleSel, COLLAPSE_TOGGLE))
	$(collapsedSel).on('hidden.bs.collapse', () => setToggleText(toggleSel, COLLAPSED_TOGGLE))

	setToggleText(
		toggleSel,
		$(`${collapsedSel}.collapse`).hasClass('show') ? COLLAPSE_TOGGLE : COLLAPSED_TOGGLE
	)
}
