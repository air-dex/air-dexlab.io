class SoftwareAdvice {
	/// @var id An ID for the software advice.
	/// @var recos List of OSes where the software is recommended

	constructor(adviceID) {
		if (typeof skill === 'string') {
			this.$ = $(`div.soft-advice[data-software="${adviceID}"]`)
		}
		else if (adviceID instanceof HTMLElement) {
			this.$ = $(adviceID)
		}
		else if (adviceID instanceof jQuery) {
			this.$ = adviceID
		}
		else {
			throw `Cannot build software advice with this argument: ${adviceID.toString()}`
		}

		this.id = this.$.attr('data-software').trim()
		this.recos = new Set(this.$.attr("data-oses").split(' '))
	}

	applyDisplay(display) {
		this.$.css('display', display)
	}

	isAdviced() {
		return this.$.css('display') !== 'none'
	}

	hasCommonOSTags(ostags) {
		let commonTags = new Set([...this.recos].filter(ostag => ostags.has(ostag)));
		return commonTags.size > 0
	}

	match(ostags) {
		return this.hasCommonOSTags(ostags)
	}

	eq(otherSA) {
		if (!(otherSA instanceof SoftwareAdvice)) {
			throw `We compare software advices, not ${typeof otherSA}: ${otherSA}.`
		}
		return this.id === otherSA.id
	}
}
