$(document).ready(function () {
	initTimeline('.timeline', {
		verticalStartPosition: 'right',
		verticalTrigger: '150px',
		forceVerticalMode: 1200
	});

	initSlider('#skrate')
	initSkillFilterForm('#skfilter__form', 'skname', 'skut', 'sktags', 'skrate', 0, 5, 'flex')
	initToggleButton('.skfilter__toggle', '#skfilter__form')
	initSkills()
	initScrollSpy('#about-me__index')
});
