#!/usr/bin/pwsh

# Whole script arguments
# Default args: default values for a given environment
Param(
	# Output directory
	[string] $output_dir = 'public',

	# Sculpin environment
	[Parameter(Mandatory=$true,HelpMessage="It must be 'dev' or 'prod'.")]
	[ValidatePattern('^(dev)|(prod)$')]
	[string] $env,

	# Should project dependencies be installed (through composer/npm install)?
	[switch] $installdeps,

	# Should vendors libraries should be installed?
	[switch] $installvendors,

	# Generating Source Maps?
	[switch] $sourcemaps,

	# Additional arguments for Sass' CLI.
	[string] $sassargs = '',

	# Additional arguments for html-minifier's CLI.
	[string] $htmlminifyargs = '',

	# Additional arguments for PostCSS' CLI.
	[string] $postcssargs = '',

	# Additional arguments for Terser's CLI.
	[string] $terserargs = '',

	# Minifying HTML. Both -minifyAll and -env prod options force it to $true.
	[switch] $minifyHTML,

	# Configuration file for HTML Minifier (relative to the project root).
	[string] $htmlMinifierConfigFile = ("config{0}minification{0}html{0}$env.json" -f [IO.Path]::DirectorySeparatorChar),

	# Minifying CSS. Both -minifyAll and -env prod options force it to $true.
	[switch] $minifyCSS,

	# Configuration file for PostCSS (relative to the project root).
	[string] $cssMinifierConfigFile = ("config{0}minification{0}css" -f [IO.Path]::DirectorySeparatorChar),

	# Minifying JavaScript. Both -minifyAll and -env prod options force it to $true.
	[switch] $minifyJS,

	# Configuration file for Uglify JS (relative to the project root).
	[string] $jsMinifierConfigFile =  ("config{0}minification{0}js{0}$env.json" -f [IO.Path]::DirectorySeparatorChar),

	# Minifying everything (HTML, CSS & JS). It sets their minifying options to $true.
	[switch] $minifyAll
)

function Install-Timeline {
	'Install Timeline ( https://squarechip.github.io/timeline/ )'
	git clone https://github.com/squarechip/timeline.git "othervendors$($sep)timeline"
}

function Install-Other {
	Install-Timeline
}

"Website begun building at $(Get-Date)"

# TODO: npx test

$project_root = $pwd.path
$sep = [IO.Path]::DirectorySeparatorChar
$debug = $env -eq 'dev'

# Node environment
if ($env -eq 'dev') {
	$node_env = 'development'
}
elseif ($env -eq 'prod') {
	$node_env = 'production'
}

# Minification settings
if ($minifyAll -or ($env -eq 'prod')) {
	$minifyHTML = $true
	$minifyCSS = $true
	$minifyJS = $true
}

####################################################################################
#                          Installing project dependencies                         #
####################################################################################
# Static site generator:                                                           #
# - Powered by Sculpin: https://sculpin.io                                         #
# - The only thing installed with Composer as of now.                              #
#                                                                                  #
# CSS processors:                                                                  #
# - Sass is the CSS preprocessor: https://sass-lang.com                            #
# - PostCSS is the CSS postprocessor: https://postcss.org                          #
#                                                                                  #
# Minifiers:                                                                       #
# - "html-minifier" for HTML: https://github.com/kangax/html-minifier              #
# - "CSS Nano" for CSS: https://cssnano.co                                         #
# - "Terser" for JavaScript : https://terser.org                                   #
#                                                                                  #
# Other dev tools installable through NPM:                                         #
# - Autoprefixer, for CSS prefixing post treatment: https://autoprefixer.github.io #
#                                                                                  #
# Other:                                                                           #
# - Timeline, a JS component for timelines: https://squarechip.github.io/timeline  #
#     - Installed by cloning its git repo                                          #
####################################################################################

if ($installdeps) {
	composer install
	npm install
	Install-Other
}

##############################
# Copying vendors CSS and JS #
##############################

if ($installvendors) {
	$assets_dest = "source$($sep)assets"
	$bootstrap_path = "node_modules$($sep)bootstrap$($sep)dist"
	$jquery_path = "node_modules$($sep)jquery$($sep)dist"
	$timeline_path = "othervendors$($sep)timeline$($sep)dist"
	$bootstrap_slider_path = "node_modules$($sep)bootstrap-slider$($sep)dist"
	$vcard_parser_path = "node_modules$($sep)vcard-parser$($sep)lib"
	$css_vendors_dest = "$assets_dest$($sep)css$($sep)vendors"
	$images_vendors_dest = "$assets_dest$($sep)images"
	$js_vendors_dest = "$assets_dest$($sep)js$($sep)vendors"

	switch ($env) {
		'dev' {
			$jquery_js = "$project_root$($sep)$jquery_path$($sep)jquery.js"
			$bootstrap_js = "$project_root$($sep)$bootstrap_path$($sep)js$($sep)bootstrap.bundle.js"
			$bootstrap_css = "$project_root$($sep)$bootstrap_path$($sep)css$($sep)bootstrap.css"
			$timeline_css = "$project_root$($sep)$timeline_path$($sep)css$($sep)timeline.min.css"
			$timeline_img = "$project_root$($sep)$timeline_path$($sep)images"
			$timeline_js = "$project_root$($sep)$timeline_path$($sep)js$($sep)timeline.min.js"
			$bootstrap_slider_css = "$project_root$($sep)$bootstrap_slider_path$($sep)css$($sep)bootstrap-slider.css"
			$bootstrap_slider_js = "$project_root$($sep)$bootstrap_slider_path$($sep)bootstrap-slider.js"
			$vcard_parser_js = "$project_root$($sep)$vcard_parser_path$($sep)vcard.js"
		}

		'prod' {
			$jquery_js = "$project_root$($sep)$jquery_path$($sep)jquery.min.js"
			$bootstrap_js = "$project_root$($sep)$bootstrap_path$($sep)js$($sep)bootstrap.bundle.min.js"
			$bootstrap_css = "$project_root$($sep)$bootstrap_path$($sep)css$($sep)bootstrap.min.css"
			$timeline_css = "$project_root$($sep)$timeline_path$($sep)css$($sep)timeline.min.css"
			$timeline_img = "$project_root$($sep)$timeline_path$($sep)images"
			$timeline_js = "$project_root$($sep)$timeline_path$($sep)js$($sep)timeline.prod.min.js"
			$bootstrap_slider_css = "$project_root$($sep)$bootstrap_slider_path$($sep)css$($sep)bootstrap-slider.min.css"
			$bootstrap_slider_js = "$project_root$($sep)$bootstrap_slider_path$($sep)bootstrap-slider.min.js"
			$vcard_parser_js = "$project_root$($sep)$vcard_parser_path$($sep)vcard.js"
		}

		default {
			throw "Environment should be 'dev' or 'prod', not $env."
		}
	}

	# Copying CSS
	($bootstrap_css, $timeline_css, $bootstrap_slider_css) | Foreach-Object {
		Copy-Item -Path "$_" -Destination "$project_root$($sep)$css_vendors_dest"
	}

	# Copying img
	# TODO: improve it.
	($timeline_img) | Foreach-Object {
		Copy-Item -Path "$_" -Recurse -Destination "$project_root$($sep)$assets_dest"
	}

	# Copying JS
	($jquery_js, $bootstrap_js, $timeline_js, $bootstrap_slider_js, $vcard_parser_js) | Foreach-Object {
		Copy-Item -Path "$_" -Destination "$project_root$($sep)$js_vendors_dest"
	}
}

###################
# Generating site #
###################

'Processing HTML...'
vendor/bin/sculpin generate --clean --output-dir="$output_dir" --env=$env -n

# HTML
if ($minifyHTML) {
	'Minifying HTML...'
	$htmlMinifierConfigFile = "$project_root$sep$htmlMinifierConfigFile"
	Push-Location "$output_dir"
	Get-ChildItem -Recurse -File `
		| Where-Object { $_.Extension -like '.html' } `
		| Foreach-Object {
			"`tMinifying $($_.FullName)"
			npx html-minifier "$($_.FullName)" -o "$($_.FullName)" -c "$htmlMinifierConfigFile"
		}
	Pop-Location
}

##################
# Processing CSS #
##################

'Processing CSS...'
Push-Location "$output_dir/assets"

# Source Map argument.
$sass_sm_arg = ($sasssourcemap -or $debug) ? '--source-map' : '--no-source-map'

# PostCSS config file folder
$postcssConfigFile = "$project_root$sep$cssMinifierConfigFile"

# Creating CSS directory. Useless for Sass' CLI.
# New-Item -ItemType Directory -Path css

# Processing CSS files from SCSS files.
$pwdpath = $pwd.path
# ^$($pwd.path)/scss/(?<shortname>.*)\.scss$
$sass_regex = "^{0}{1}scss{1}(?<shortname>.*)\.scss$" -f [Regex]::Escape($pwdpath),[Regex]::Escape($sep)
$useCSSminifier = $minifyCSS ? '--use cssnano' : ''

(Get-ChildItem -Recurse -Path scss) `
	| Where-Object { $_.FullName -match "$sass_regex" -and -not $_.Name.StartsWith('_') } `
	| Foreach-Object {
		# Processing one stylesheet. Sass time!
		$sass_file = "scss$sep$($Matches.shortname).scss"
		$css_file = "css$sep$($Matches.shortname).css"
		"Processing $pwdpath$sep$css_file"
		"`tCreating from $pwdpath$sep$sass_file"
		npx sass $sass_file $css_file $sass_sm_arg $sassargs

		"`tPrefixing$($minifyCSS ? ' and minifying' : '')"
		npx postcss $css_file `
			--use autoprefixer $useCSSminifier `
			-o $css_file `
			--config "$postcssConfigFile" `
			--env $node_env `
			--verbose `
			$postcssargs
	}

# Removing useless Sass files
if (-not $debug) {
	Remove-Item -Recurse -Path scss
}

Pop-Location

# JavaScript minification
if ($minifyJS) {
	'Minifying JavaScript...'
	$jsMinifierConfigFile = "$project_root$sep$jsMinifierConfigFile"

	$debugOpts = ''
	if ($env -eq 'dev') {
		$debugOpts = '--timings'
	}

	Push-Location "$output_dir$($sep)assets$($sep)js"
	Get-ChildItem -Recurse -File `
		| Where-Object {
			$_.Extension -eq '.js' `
				-and -not ($_.FullName -match "$($pwd.path)$($sep)vendors$sep*")
		} `
		| Foreach-Object {
			"`tMinifying $($_.FullName)"
			npx terser "$($_.FullName)" -o "$($_.FullName)" --config-file "$jsMinifierConfigFile"
		}
	Pop-Location
}

###########
# The end #
###########

"Website ended building at $(Get-Date)"
