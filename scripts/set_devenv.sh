#!/bin/bash

####################################################################
# @file set_devenv.sh                                              #
# @brief BASH script for setting dev environment on Ubuntu         #
# What is required there?                                          #
# - PHP (7 or later), because of Composer and Sculpin.             #
# - Composer, required by Sculpin.                                 #
# - Node.js, to install Sass (and perhaps other tools later too?). #
# - NPM, for installing (Dart) Sass.                               #
# - Sass, through Dart Sass.                                       #
# - Powershell, for being able to run the build script.            #
# @author Romain DUCHER <ducher.romain@gmail.com>                  #
####################################################################

# For tzdata purposes
export DEBIAN_FRONTEND=noninteractive
export TZ=Europe/Paris

echo '##################################################'
echo '# Install Node.js and NPM through Nodesource PPA #'
echo '# (Bionic version is too old)                    #'
echo '##################################################'
apt-get update
apt-get install -y wget
wget -qO- https://deb.nodesource.com/setup_lts.x | bash -
apt-get update
apt-get install -y nodejs
echo 'Node.js and NPM are installed.'

echo '###########################################'
echo '# Installing  APT packages                #'
echo '# (related to PHP, Composer and Timeline) #'
echo '###########################################'
apt-get update

# PHP-only stuff
PHP_PKG='php'

# Composer packages:
# - unzip cmd.
# - "zip" PHP extension
COMPOSER_PKG='composer php-zip unzip'

# PHP extensions required by Composer dependencies:
# - "mbstring" by Sculpin.
# - "intl" by some Symfony deps.
# - "DOM" (in php-xml) to generate some autoload files
COMPOSER_DEPS_PKG='php-mbstring  php-intl php-xml'

# "Timeline" JS component is git-cloned into the project.
OTHER_PKG="git"

# Install!
apt-get install -y $PHP_PKG $COMPOSER_PKG $COMPOSER_DEPS_PKG $OTHER_PKG

echo 'Packages are installed.'
echo 'Dev tools installable through NPM, Composer or git will be installed in /scripts/build_site.ps1.'

echo '##################################################################################'
echo '# Installing Powershell                                                          #'
echo '# Using MS process:                                                              #'
echo '#   https://docs.microsoft.com/fr-fr/powershell/scripting/install/install-ubuntu #'
echo '##################################################################################'

# Powershell install requires the following packages: wget software-properties-common
PWSH_INSTALL_PKG='wget software-properties-common'
apt-get install -y $PWSH_INSTALL_PKG
apt-get update

# Download the Microsoft repository GPG keys
wget -q https://packages.microsoft.com/config/ubuntu/22.04/packages-microsoft-prod.deb

# Register the Microsoft repository GPG keys
dpkg -i packages-microsoft-prod.deb

# Update the list of products
apt-get update

# Enable the "universe" repositories
add-apt-repository universe

# Install PowerShell
apt-get install -y powershell

echo 'Powershell is installed.'
