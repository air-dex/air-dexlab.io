const DEV_ENV = 'development'
const PROD_ENV = 'production'

module.exports = (ctx) => ({
	map: ctx.options.map,
	plugins: {
		'autoprefixer': {
			env: ctx.env,
			cascade: ctx.env === DEV_ENV,
			remove: false,
			ignoreUnknownVersions: true
		},
		'cssnano': {
			preset: ['default', {
				cssDeclarationSorter: false,
				calc: false,
				convertValues: {
					length: false
				},
				discardComments: {
					// TODO: Fix it for source maps. Will be done when revamping build script values.
					remove: (comment) => {
						console.log(`Map ? ${ctx.options.map}`)
						console.log(ctx.options.map)
						let res = ctx.options.map && !comment.startsWith('# sourceMappingURL=') || !ctx.options.map
						console.log(`Remove ${comment} ? ${res}`)
						return res;
					}
				},
				minifyFontValues: {
					removeQuotes: false
				},
				minifyGradients: false,
				normalizeDisplayValues: false,
				normalizePositions: false,
				normalizeString: {
					preferedQuote: "double"
				},
				normalizeUrl: false,
				reduceInitial: false,
				svgo: false
			}]
		}
	}
})
